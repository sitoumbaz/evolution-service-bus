package eu.chorevolution.vsb.clientserveur.coapplayground_pushnotification;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapHandler;
import org.eclipse.californium.core.CoapObserveRelation;
import org.eclipse.californium.core.CoapResponse;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class CoapObserver{

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String coapUri = "coap://127.0.0.1:8893/bridgeNextClosure";
		CoapClient client = new CoapClient(coapUri); 
		System.out.println("press enter to exit"); 
		CoapObserveRelation relation = client.observe( 
				   
				    new CoapHandler() { 
				     @Override public void onLoad(CoapResponse response){ 
				      String content = response.getResponseText(); 
				      System.out.println("GET NOTIFICATION: " + content); 
				      JSONParser parser = new JSONParser();
					  JSONObject jsonObject = null;
//
//						try{
//							jsonObject = (JSONObject) parser.parse(content);
//						} catch (ParseException e) {
//							e.printStackTrace();
//						}
//						
//						String op_name = (String)jsonObject.get("op_name");
//						String lat = (String)jsonObject.get("lat");
//						String lon = (String)jsonObject.get("lon");
//						System.out.println("op_name="+op_name+"&lat="+lat+"&lon="+lon);
						
				     } 
				     @Override public void onError(){
				    	 
				      System.err.println("OBSERVING FAILED (press enter to exit)"); 
				      
				     } 
				    }); 
		   
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); 
		try { br.readLine(); } catch (IOException e) { }
		
	}

}
