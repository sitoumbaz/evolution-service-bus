/*package eu.chorevolution.vsb.playgrounds.clientserver.rest.experiment;


import org.restlet.Client;
import org.restlet.Component;
import org.restlet.Request;
import org.restlet.Server;
import org.restlet.data.MediaType;
import org.restlet.data.Method;
import org.restlet.data.Protocol;
import eu.chorevolution.vsb.agent.MeasureAgent;
import eu.chorevolution.vsb.monitor.util.MonitorConstant;

public class StartRestClient {

	private Client client = null;
	private MeasureAgent agent = null;
	private Server printerServer = null;
	private Component printerComponent = null;

	public StartRestClient() {
		
		agent = new MeasureAgent("timestamp_1",System.currentTimeMillis(),MonitorConstant.MDefault,MonitorConstant.timestamp_1_port_listener);
		client = new Client(Protocol.HTTP);
		this.printerServer = new Server(Protocol.HTTP, 8081);
		this.printerComponent = new Component();
		this.printerComponent.getServers().add(printerServer);
	}

	public void send(String message) {

		Request request = new Request();
		request.setResourceRef("http://"+MonitorConstant.MDefault+":8080/");
		request.setMethod(Method.POST);
		request.setEntity(message, MediaType.APPLICATION_JSON);
		String message_id = agent.getMessageID(message);
		agent.fire(""+System.currentTimeMillis()+"-"+message_id);
		client.handle(request);

	}

}
*/