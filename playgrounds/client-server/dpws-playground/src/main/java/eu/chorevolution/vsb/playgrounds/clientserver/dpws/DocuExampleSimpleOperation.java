package eu.chorevolution.vsb.playgrounds.clientserver.dpws;

import org.ws4d.java.communication.CommunicationException;
import org.ws4d.java.schema.Element;
import org.ws4d.java.schema.SchemaUtil;
import org.ws4d.java.security.CredentialInfo;
import org.ws4d.java.service.InvocationException;
import org.ws4d.java.service.Operation;
import org.ws4d.java.service.parameter.ParameterValue;
import org.ws4d.java.service.parameter.ParameterValueManagement;
import org.ws4d.java.types.QName;

import eu.chorevolution.vsb.playgrounds.clientserver.dpws.DocuExampleDevice;


public class DocuExampleSimpleOperation extends Operation {
	
	public final static String	NAME	= "name";
	public final static String	REPLY	= "reply";
	
	public DocuExampleSimpleOperation() {
		// TODO Auto-generated constructor stub
		super("DocuExampleSimpleOperation", new QName("BasicServices", DocuExampleDevice.DOCU_NAMESPACE));
		
		Element nameElem = new Element(new QName(NAME, DocuExampleDevice.DOCU_NAMESPACE), SchemaUtil.TYPE_STRING);
		setInput(nameElem);
		Element reply = new Element(new QName(REPLY, DocuExampleDevice.DOCU_NAMESPACE), SchemaUtil.TYPE_STRING);
		setOutput(reply);
	}
	@Override
	protected ParameterValue invokeImpl(ParameterValue arg0, CredentialInfo arg1)
			throws InvocationException, CommunicationException {
		// TODO Auto-generated method stub
		
		String name = ParameterValueManagement.getString(arg0, "name");
		System.err.println("Get request : name="+name);
		ParameterValue result = createOutputValue(); 
		ParameterValueManagement.setString(result, REPLY, "How are you "+name+" ? ");
		System.err.println("Send response : "+REPLY+"="+"How are you "+name+" ? ");
		return result;
	}

}
