package eu.chorevolution.vsb.tools.monitor.mbeans;

import java.io.IOException;

import javax.management.MBeanNotificationInfo;

public interface ExperimentMBean {

	public boolean getExperimentRunning() throws IOException;

	public void setExperimentRunning(boolean experimentRunning) throws IOException;

	public long getExperimentStartTime() throws IOException;

	public void setExperimentStartTime(long experimentStartTime) throws IOException;
	
	public long getExperimentMsgCounter() throws IOException;

	public void setExperimentMsgCounter(long experimentMsgCounter) throws IOException;
	
	
	public long getExperimentCompleteTime() throws IOException;

	public void setExperimentCompleteTime(long experimentCompleteTime) throws IOException;
	
	public double getExperimentAvgMessagesSent() throws IOException;

	public void setExperimentAvgMessagesSent(double experimentAvgMessagesSent) throws IOException;
	
	
	public long getExperimentDuration() throws IOException;

	public void setExperimentDuration(long experimentDuration) throws IOException;
	
	
	
	public abstract MBeanNotificationInfo[] getNotificationInfo();
}
