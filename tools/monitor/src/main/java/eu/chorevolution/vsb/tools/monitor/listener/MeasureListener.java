package eu.chorevolution.vsb.tools.monitor.listener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.management.AttributeChangeNotification;
import javax.management.InstanceNotFoundException;
import javax.management.ListenerNotFoundException;
import javax.management.MBeanServerConnection;
import javax.management.MBeanServerInvocationHandler;
import javax.management.MalformedObjectNameException;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import eu.chorevolution.vsb.tools.monitor.mbeans.MeasureMBean;

public class MeasureListener implements NotificationListener {

	private MBeanServerConnection mBeanServerConnection = null;
	private JMXConnector connector = null;
	private ObjectName objectName = null;
	private int port = 9000;
	private String host = "localhost";
	private String attribute = null;
	private HashMap<Integer, Long[]> timestampMap;

	public MeasureListener(String attribute, String host, int port, HashMap<Integer, Long[]> timestampMap) {

		this.attribute = attribute;
		this.port = port;
		this.host = host;
		this.timestampMap = timestampMap;

	}

	@Override
	public void handleNotification(Notification notification, Object handback) {
		// TODO Auto-generated method stub
		// System.out.println("Notification received :");
		// System.out.print("\tClassName: " +
		// notification.getClass().getName());
		// System.out.print("\tSource: " + notification.getSource());
		// System.out.print("\tType: " + notification.getType());
		String message = (String) notification.getMessage();
		// System.out.print("\tMessage: " + message.toString());
		// System.out.println("");

		if (notification instanceof javax.management.AttributeChangeNotification) {

			final AttributeChangeNotification acn = (AttributeChangeNotification) notification;
			// System.out.println("\tAttributeName: " + acn.getAttributeName());
			// System.out.println("\tAttributeType: " + acn.getAttributeType());

			final Object new_value = acn.getNewValue();
			// final Object old_value = acn.getOldValue();
			String timestamp_built = (String) new_value;

			String array[] = timestamp_built.split("-");
			Long timestamp = Long.valueOf(array[0]).longValue();
			int message_id = Integer.valueOf(array[1]);
			String timestamp_name = array[2];
			Long timestamp_array[] = new Long[] { 0l, 0l, 0l, 0l, 0l, 0l };

			if (!timestampMap.containsKey(message_id)) {

				synchronized (timestampMap) {

					timestampMap.put(message_id, timestamp_array);
				}
			}
			switch (timestamp_name) {
			case "timestamp_1":

				synchronized (timestampMap){

					Long[] time_array = timestampMap.get(message_id);
					if (time_array != null) {

						if (time_array[0] == 0l) {

							time_array[0] = timestamp;
							timestampMap.put(message_id, time_array);

						}

					} else {

						time_array = new Long[6];
						timestamp_array[0] = timestamp;
						timestampMap.put(message_id, time_array);
					}

				}

				break;
			case "timestamp_2":

				synchronized (timestampMap) {

					Long[] time_array1 = timestampMap.get(message_id);
					if (time_array1 != null) {

						if (time_array1[1] == 0l) {

							time_array1[1] = timestamp;
							timestampMap.put(message_id, time_array1);

						}

					} else {

						time_array1 = new Long[6];
						time_array1[1] = timestamp;
						timestampMap.put(message_id, time_array1);
					}

				}

				break;

			case "timestamp_3":

				synchronized (timestampMap) {

					Long[] time_array2 = timestampMap.get(message_id);
					if (time_array2 != null) {

						if (time_array2[2] == 0l) {

							time_array2[2] = timestamp;
							timestampMap.put(message_id, time_array2);

						}

					} else {

						time_array2 = new Long[6];
						time_array2[2] = timestamp;
						timestampMap.put(message_id, time_array2);
					}

				}

				break;

			case "timestamp_4":

				synchronized (timestampMap) {

					Long[] time_array3 = timestampMap.get(message_id);
					if (time_array3 != null) {

						if (time_array3[3] == 0l) {

							time_array3[3] = timestamp;
							timestampMap.put(message_id, time_array3);

						}

					} else {

						time_array3 = new Long[6];
						time_array3[3] = timestamp;
						timestampMap.put(message_id, time_array3);
					}

				}

				break;
			case "timestamp_5":

				synchronized (timestampMap) {

					Long[] time_array4 = timestampMap.get(message_id);
					if (time_array4 != null) {

						if (time_array4[4] == 0l) {

							time_array4[4] = timestamp;
							timestampMap.put(message_id, time_array4);

						}

					} else {

						time_array4 = new Long[6];
						time_array4[4] = timestamp;
						timestampMap.put(message_id, time_array4);
					}
				}

				break;
			case "timestamp_6":

				synchronized (timestampMap) {

					Long[] time_array5 = timestampMap.get(message_id);
					if (time_array5 != null) {

						if (time_array5[5] == 0l) {

							time_array5[5] = timestamp;
							timestampMap.put(message_id, time_array5);

						}

					} else {

						time_array5 = new Long[6];
						time_array5[5] = timestamp;
						timestampMap.put(message_id, time_array5);
					}
				}

				break;
			default:
				break;
			}

		}

	}

	public void disconnect() {

		try {

			System.out.println("Unsubscribe to " + objectName.toString());
			mBeanServerConnection.removeNotificationListener(objectName, this);
			System.out.println("Disconnection from server");
			connector.close();

		} catch (InstanceNotFoundException | ListenerNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void connect() {

		try {

			objectName = new ObjectName("eu.chorevolution.vsb.tools.monitor.mbeans:type=MeasureMBean,name=" + attribute);
			JMXServiceURL url = new JMXServiceURL("service:jmx:rmi:///jndi/rmi://" + host + ":" + port + "/server");
			connector = JMXConnectorFactory.connect(url, null);
			mBeanServerConnection = connector.getMBeanServerConnection();
			MeasureMBean measureMBean = (MeasureMBean) MBeanServerInvocationHandler
					.newProxyInstance(mBeanServerConnection, objectName, MeasureMBean.class, false);
			String timestamp = measureMBean.getTimestamp();
			System.out.println("Current mbean value = " + timestamp);
			System.out.println("Subscribe to NotificationListener" + objectName.toString());
			mBeanServerConnection.addNotificationListener(objectName, this, null, null);

		} catch (MalformedObjectNameException | IOException | InstanceNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
