package eu.chorevolution.vsb.tools.monitor.mbeans;

import java.io.IOException;

import javax.management.MBeanNotificationInfo;

public interface MeasureMBean {

	public String getTimestamp() throws IOException;

	public void setTimestamp(String timestamp) throws IOException;

	public String getName() throws IOException;

	public void setName(String name) throws IOException;

	public abstract MBeanNotificationInfo[] getNotificationInfo();
}
