package eu.chorevolution.vsb.tools.monitor.listener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.management.AttributeChangeNotification;
import javax.management.InstanceNotFoundException;
import javax.management.ListenerNotFoundException;
import javax.management.MBeanServerConnection;
import javax.management.MBeanServerInvocationHandler;
import javax.management.MalformedObjectNameException;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import javax.swing.SwingConstants;

import eu.chorevolution.vsb.tools.monitor.mbeans.ExperimentMBean;
import eu.chorevolution.vsb.tools.monitor.mbeans.MeasureMBean;

public class ExperimentListener implements NotificationListener {

	private MBeanServerConnection mBeanServerConnection = null;
	private JMXConnector connector = null;
	private ObjectName objectName = null;
	private String port = "9000";
	private String host = "localhost";
	private String attribute = null;
	public long experimentStartTime = 0l;
	public long experimentMsgCounter = 0l;
	public long experimentCompleteTime = 0l;
	public long experimentDuration = 0l;
	public double experimentAvgMessagesSent = 0.0;
	public boolean experimentRunning = true;
	// public long

	public ExperimentListener(String attribute, String host, String port) {

		this.attribute = attribute;
		this.port = port;
		this.host = host;

	}

	@Override
	public void handleNotification(Notification notification, Object handback) {

		Object message = notification.getMessage();

		if (notification instanceof javax.management.AttributeChangeNotification) {

			AttributeChangeNotification acn = (AttributeChangeNotification) notification;
			Object AttributeName = acn.getAttributeName();
			Object new_value = acn.getNewValue();
			// final Object old_value = acn.getOldValue();
			// System.out.println("AttributeType :" + acn.getAttributeType());
			// System.out.println("AttributeName :" + AttributeName);
			// System.out.println("new_value :" + new_value);

			if (AttributeName.equals("experimentRunning")) {

				System.out.println("Catch event experimentRunning");
				experimentRunning = (boolean) new_value;

			} else if (AttributeName.equals("experimentStartTime")) {

				System.out.println("Catch event experimentStartTime");
				experimentStartTime = (long) new_value;
			}

			else if (AttributeName.equals("experimentMsgCounter")) {

				System.out.println("Catch event experimentMsgCounter");
				experimentMsgCounter = (long) new_value;
			}

			else if (AttributeName.equals("experimentCompleteTime")) {

				System.out.println("Catch event experimentCompleteTime");
				experimentCompleteTime = (long) new_value;
			}
			
			else if (AttributeName.equals("experimentAvgMessagesSent")) {

				System.out.println("Catch event experimentAvgMessagesSent");
				experimentAvgMessagesSent = (double) new_value;
			}
			else if(AttributeName.equals("experimentDuration")){
				
				System.out.println("Catch event experimentDuration");
				experimentDuration = (long) new_value;
			}
			else {

				System.out.println("Not good");
			}

		}

		// System.out.println("experimentRunning " + experimentRunning);
		// System.out.println("experimentStartTime " + experimentStartTime);
		// System.out.println("experimentMsgCounter " + experimentMsgCounter);

	}

	public void disconnect() {

		try {

			System.out.println("Unsubscribe to " + objectName.toString());
			mBeanServerConnection.removeNotificationListener(objectName, this);
			System.out.println("Disconnection from server");
			connector.close();

		} catch (InstanceNotFoundException | ListenerNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void connect() {

		try {

			objectName = new ObjectName("eu.chorevolution.vsb.tools.monitor.mbeans:type=ExperimentMBean,name=" + attribute);
			JMXServiceURL url = new JMXServiceURL("service:jmx:rmi:///jndi/rmi://" + host + ":" + port + "/server");
			connector = JMXConnectorFactory.connect(url, null);
			mBeanServerConnection = connector.getMBeanServerConnection();
			ExperimentMBean experimentMBean = (ExperimentMBean) MBeanServerInvocationHandler
					.newProxyInstance(mBeanServerConnection, objectName, ExperimentMBean.class, false);
			long experimentStartTime = experimentMBean.getExperimentStartTime();
			System.out.println("Current mbean value = " + experimentStartTime);
			System.out.println("Subscribe to NotificationListener" + objectName.toString());
			mBeanServerConnection.addNotificationListener(objectName, this, null, null);

		} catch (MalformedObjectNameException | IOException | InstanceNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
