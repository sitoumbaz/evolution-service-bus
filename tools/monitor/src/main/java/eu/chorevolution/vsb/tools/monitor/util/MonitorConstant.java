package eu.chorevolution.vsb.tools.monitor.util;

public class MonitorConstant {
	
//	public static final String TIME_SERVER = "128.93.64.246";
//	public static final String M1  = "128.93.65.234";
//	public static final String M2  = "128.93.65.233";
//	public static final String M3  = "128.93.64.1";
//	public static final String M4  = "128.93.64.90";
//	public static final String MDefault  = "127.0.0.1";
	
	public static final String TIME_SERVER = "192.168.0.101";
	public static final String M1  = "127.0.0.1";
	public static final String M2  = "127.0.0.1";
	public static final String M3  = "127.0.0.1";
	public static final String M4  = "127.0.0.1";
	public static final String MDefault  = "127.0.0.1";
	
	public static final int TIME_SERVER_PORT = 9999;
	public static final int timestamp_1_port_listener = 9000;
	public static final int timestamp_2_port_listener = 9001;
	public static final int timestamp_3_port_listener = 9002;
	public static final int timestamp_4_port_listener = 9003;
	public static final int timestamp_5_port_listener = 9004;
	public static final int timestamp_6_port_listener = 9005;
	
}
