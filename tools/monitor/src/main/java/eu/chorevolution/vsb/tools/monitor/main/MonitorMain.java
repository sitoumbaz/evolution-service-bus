package eu.chorevolution.vsb.tools.monitor.main;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import eu.chorevolution.vsb.tools.monitor.listener.ExperimentListener;
import eu.chorevolution.vsb.tools.monitor.listener.MeasureListener;
import eu.chorevolution.vsb.tools.monitor.util.MonitorConstant;

/**
 * Unit test for simple App.
 */

public class MonitorMain {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
			
		
		if (args.length != 1){

			System.err.println("Missing arguments");
			System.err.println("java -jar MonitorMain-jar-with-dependencies.jar  steadystateTimer");
			System.exit(0);
		}
		
//		long waitSteadystate = 1000; 
		long waitMessagestate = Long.valueOf(args[0]);
		ExperimentListener experimentListener = new ExperimentListener("StartExperiment", MonitorConstant.M3, "9006");
		
		
//		
//		MeasureListener listener_0 = new MeasureListener("timestamp_1", "localhost", "9000");
//		MeasureListener listener_1 = new MeasureListener("timestamp_2", "localhost", "9001");
//		MeasureListener listener_2 = new MeasureListener("timestamp_3", "localhost", "9002");
//		MeasureListener listener_3 = new MeasureListener("timestamp_4", "localhost", "9003");
//		MeasureListener listener_4 = new MeasureListener("timestamp_5", "localhost", "9004");
//		ExperimentListener experimentListener = new ExperimentListener("StartExperiment", "localhost", "9006");
//		MeasureListener listener_5 = new MeasureListener("timestamp_6", "localhost", "9005");
		
		HashMap<Integer, Long[]> timestampMap = new HashMap<Integer, Long[]>();
		
//		MeasureListener listener_0 = new MeasureListener("timestamp_1", MonitorConstant.MDefault, MonitorConstant.timestamp_1_port_listener,timestampMap);
//		MeasureListener listener_1 = new MeasureListener("timestamp_2", MonitorConstant.MDefault, MonitorConstant.timestamp_2_port_listener,timestampMap);
//		MeasureListener listener_2 = new MeasureListener("timestamp_3", MonitorConstant.MDefault, MonitorConstant.timestamp_3_port_listener,timestampMap);
//		MeasureListener listener_3 = new MeasureListener("timestamp_4", MonitorConstant.MDefault, MonitorConstant.timestamp_4_port_listener,timestampMap);
//		MeasureListener listener_4 = new MeasureListener("timestamp_5", MonitorConstant.MDefault, MonitorConstant.timestamp_5_port_listener,timestampMap);
//		MeasureListener listener_5 = new MeasureListener("timestamp_6", MonitorConstant.MDefault, MonitorConstant.timestamp_6_port_listener,timestampMap);
		
		System.out.println("Starting...");
		new java.util.Timer().schedule(new java.util.TimerTask() {

			@Override
			public void run() {

				// TODO Auto-generated method stub
				long t0 = 0, t1 = 0, t2, t3 = 0, t4 = 0, t5 = 0;
				
				MeasureListener listener_0 = new MeasureListener("timestamp_1", MonitorConstant.M3, MonitorConstant.timestamp_1_port_listener,timestampMap);
				MeasureListener listener_1 = new MeasureListener("timestamp_2", MonitorConstant.M2, MonitorConstant.timestamp_2_port_listener,timestampMap);
				MeasureListener listener_2 = new MeasureListener("timestamp_3", MonitorConstant.M2, MonitorConstant.timestamp_3_port_listener,timestampMap);
				MeasureListener listener_3 = new MeasureListener("timestamp_4", MonitorConstant.M4, MonitorConstant.timestamp_4_port_listener,timestampMap);
				MeasureListener listener_4 = new MeasureListener("timestamp_5", MonitorConstant.M4, MonitorConstant.timestamp_5_port_listener,timestampMap);
				MeasureListener listener_5 = new MeasureListener("timestamp_6", MonitorConstant.M3, MonitorConstant.timestamp_6_port_listener,timestampMap);
				
				int style = DateFormat.MEDIUM;
				DateFormat df = DateFormat.getDateInstance(style, Locale.US);
			    
				 
				listener_0.connect();
				listener_1.connect();
				listener_2.connect();
				listener_3.connect();
				listener_4.connect();
				listener_5.connect();
				experimentListener.connect();

				try{
					
					Thread.sleep(1000);
					
				} catch (InterruptedException e){
					
					e.printStackTrace();
				}

				
				long experimentMsgReceive = 0l;
				long experimentMsgReceiveBestFitnesss = 0l;
				System.out.print("Start Monitoring.");
				while (experimentListener.experimentRunning){
					
					
					System.out.print(".");
					try {
						Thread.sleep(10000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					
				}
				
				try {
					System.out.print("Waiting for "+waitMessagestate);
					Thread.sleep(waitMessagestate);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

							
//				MongoClient mongoClient = new MongoClient(new MongoClientURI("mongodb://localhost:27017"));
//				DB database = mongoClient.getDB("experiments");
//				DBCollection collection = database.getCollection("exp_websocket_rest_mqtt_05042017_585_100_distributed");
				
				Date start = new Date(experimentListener.experimentStartTime);
				Date complete = new Date(experimentListener.experimentCompleteTime);
				long BC1 = 0l;
				long BC2 = 0l;
				long EtoE = 0l;
				long StoBC1 = 0l;
				long BC1toBC2 = 0l;
				long BC2toR = 0l;
// System.out.print("\t\t t0 \t\t   \t\t t1 \t\t   \t\t t2 \t\t \t\t t3 \t\t  \t\t t4 \t\t  \t\t t5 \t\t  \t\t t1-t0 \t\t  \t\t t2-t1 \t\t  \t\t t3-t2 \t\t   \t\t t4-t3 \t\t   \t\t t5-t4 \t\t   \t\t t5-t0 \t\t\n");
				
			
				Set set = timestampMap.entrySet();
				Iterator iterator = set.iterator();
				while (iterator.hasNext()) {
					
					if(experimentMsgReceive > 100){
						
						t0 = 0;
						t1 = 0;
						t2 = 0;
						t3 = 0;
						t4 = 0;
						t5 = 0;
						Map.Entry mentry = (Map.Entry) iterator.next();
						//System.out.print("Message ID : " + mentry.getKey()+"\t" );
						Long[] time_array = (Long[]) mentry.getValue();
						t0 = time_array[0];
						t1 = time_array[1];
						t2 = time_array[2];
						t3 = time_array[3];
						t4 = time_array[4];
						t5 = time_array[5];
						if(t0 != 0 && t1 != 0 && t2 != 0 && t3 != 0 && t4 != 0 && t5 != 0 ){
							
							BC1 += (t2 - t1);
							BC2 += (t4 - t3);
							EtoE += (t5 - t0);
							StoBC1 += (t1 - t0);
							BC1toBC2 += (t3 - t2);
							BC2toR += (t5 - t4);
//							System.out.print(t0 + "\t" + t1 + "\t" + t2 + "\t" + t3 + "\t" + t4 + "\t" + t5 + "\t" + (t1 - t0) + "\t"
//									+ (t2 - t1) + "\t" + (t3 - t2) + "\t" + (t4 - t3) + "\t" + (t5 - t4) + "\t" + (t5 - t0) + "\n");
							experimentMsgReceiveBestFitnesss++;
							
						}
					}
					
					experimentMsgReceive++;	
				}
			      
//					DBObject doc = new BasicDBObject().append("timestamp_0", t0).append("timestamp_1", t1)
//							.append("timestamp_2", t2).append("timestamp_3", t3).append("timestamp_4", t4)
//							.append("timestamp_5", t5).append("latency_bc_1", (t2 - t1)).append("latency_bc_2", (t4 - t3))
//							.append("latency_end_to_end", (t5 - t0));
//					collection.insert(doc);

				
				long AVG_BC1 = BC1 / (experimentMsgReceiveBestFitnesss);
				long AVG_BC2 = BC2 / (experimentMsgReceiveBestFitnesss);
				long AVG_EtoE = EtoE / (experimentMsgReceiveBestFitnesss);
				long AVG_StoBC1 = StoBC1 / (experimentMsgReceiveBestFitnesss);
				long AVG_BC1toBC2 = BC1toBC2 / (experimentMsgReceiveBestFitnesss);
				long AVG_BC2toR = BC2toR / (experimentMsgReceiveBestFitnesss);
				if(experimentMsgReceive > experimentListener.experimentMsgCounter ){
					
					
					experimentMsgReceive = experimentListener.experimentMsgCounter;
				}
				System.out.println("\n=========================================================== ");
				System.out.println("Start time: " +start );
				System.out.println("Finish time: " +complete );
				System.out.println("Messages sent: " + experimentListener.experimentMsgCounter );
				System.out.println("Messages receive: " + experimentMsgReceive);
				System.out.println("Messages lost: " + (experimentListener.experimentMsgCounter - experimentMsgReceive) );
				System.out.println("Messages take for computing : " +experimentMsgReceiveBestFitnesss );
				long effectiveDuration = (long)(1200000  + waitMessagestate);
				System.out.println("Monitoring duration : " +effectiveDuration );
				System.out.println("Throughput  : " + (double)( (double)experimentMsgReceive /(double)effectiveDuration) );
				System.out.println("Losses : " + ( (double) ((double)experimentMsgReceive /  (double) experimentListener.experimentMsgCounter)));
				System.out.println("Message / seconde : " + experimentListener.experimentAvgMessagesSent );
				System.out.println("BC1 Avg latency: " + AVG_BC1 );
				System.out.println("BC2 Avg latency: " + AVG_BC2 );
				System.out.println("End to End Avg latency: " + AVG_EtoE );
				System.out.println("Sender t0 BC1 Avg latency: " + AVG_StoBC1 );
				System.out.println("BC1 t0 BC2 Avg latency: " + AVG_BC1toBC2 );
				System.out.println("BC2 t0 Receiver Avg latency: " + AVG_BC2toR );
				System.out.println("=========================================================== \n");
				System.out.println(""+experimentListener.experimentMsgCounter+"\t"+experimentMsgReceive+"\t"+AVG_StoBC1+"\t"+AVG_BC1toBC2+"\t"+AVG_BC2toR+"\t"+AVG_BC1+"\t"+AVG_BC2+"\t"+AVG_EtoE);
			
				listener_0.disconnect();
				listener_1.disconnect();
				listener_2.disconnect();
				listener_3.disconnect();
				listener_4.disconnect();
				listener_5.disconnect();
			}
			
			
			
		}, 10000);

	}
	
	

}
