package eu.chorevolution.vsb.experiment.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import org.json.simple.JSONObject;

public class MessageGenerator {
	
	public static int length;
	public long message_id = 0;
	
	public MessageGenerator(){
		
		message_id = 0;
	}
	
	public String getMessage(){
		
		return 	getMessage5000bytes();
	}
	
	public String getSmallMessage250bytes(){

		long LAT = Thread.currentThread().getId();
		long LON = Thread.currentThread().getId();
		long TEMP = Thread.currentThread().getId();

		JSONObject jsonObject = new JSONObject();
		
		jsonObject.put("lat", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_1", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_1", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_1", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_2", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_2", String.valueOf((LAT * System.nanoTime())));
		jsonObject.put("temp_2", String.valueOf((TEMP * System.nanoTime())));

//		jsonObject.put("lat_3", String.valueOf((LAT * System.currentTimeMillis())));
//		jsonObject.put("lon_3", String.valueOf((LON * System.nanoTime())));
//		jsonObject.put("temp_3", String.valueOf((TEMP * System.nanoTime())));
//
//		jsonObject.put("lat_4", String.valueOf((LAT * System.currentTimeMillis())));
//		jsonObject.put("lon_4", String.valueOf((LAT * System.nanoTime())));
//		jsonObject.put("temp_4", String.valueOf((TEMP * System.nanoTime())));
//////
//		jsonObject.put("lat_5", String.valueOf((LAT * System.currentTimeMillis())));
//		jsonObject.put("lon_5", String.valueOf((LON * System.nanoTime())));
//		jsonObject.put("temp_5", String.valueOf((TEMP * System.nanoTime())));
//
//		jsonObject.put("lat_6", String.valueOf((LAT * System.currentTimeMillis())));
//		jsonObject.put("lon_6", String.valueOf((LON * System.nanoTime())));
//		jsonObject.put("temp_6", String.valueOf((TEMP * System.nanoTime())));
////
//		jsonObject.put("lat_7", String.valueOf((LAT * System.currentTimeMillis())));
//		jsonObject.put("lon_7", String.valueOf((LON * System.nanoTime())));
//		jsonObject.put("temp_7", String.valueOf((TEMP * System.nanoTime())));
//
//		jsonObject.put("lat_8", String.valueOf((LAT * System.currentTimeMillis())));
//		jsonObject.put("lon_8", String.valueOf((LON * System.nanoTime())));
//		jsonObject.put("temp_8", String.valueOf((TEMP * System.nanoTime())));
//
//		jsonObject.put("lat_9", String.valueOf((LAT * System.currentTimeMillis())));
//		jsonObject.put("lon_9", String.valueOf((LON * System.nanoTime())));
//		jsonObject.put("temp_9", String.valueOf((TEMP * System.nanoTime())));

//		jsonObject.put("lat_10", String.valueOf((LAT * System.currentTimeMillis())));
//		jsonObject.put("lon_10", String.valueOf((LON * System.nanoTime())));
//		jsonObject.put("temp_10", String.valueOf((TEMP * System.nanoTime())));
//
//		jsonObject.put("lat_11", String.valueOf((LAT * System.currentTimeMillis())));
//		jsonObject.put("lon_11", String.valueOf((LON * System.nanoTime())));
//		jsonObject.put("temp_11", String.valueOf((TEMP * System.nanoTime())));
//
//		jsonObject.put("lat_12", String.valueOf((LAT * System.currentTimeMillis())));
//		jsonObject.put("lon_12", String.valueOf((LON * System.nanoTime())));
//		jsonObject.put("temp_12", String.valueOf((TEMP * System.nanoTime())));
//
//		jsonObject.put("lat_13", String.valueOf((LAT * System.currentTimeMillis())));
//		jsonObject.put("lon_13", String.valueOf((LON * System.nanoTime())));
//		jsonObject.put("temp_13", String.valueOf((TEMP * System.nanoTime())));
//
//		jsonObject.put("lat_14", String.valueOf((LAT * System.currentTimeMillis())));
//		jsonObject.put("lon_14", String.valueOf((LON * System.nanoTime())));
//		jsonObject.put("temp_14", String.valueOf((TEMP * System.nanoTime())));
//
//		jsonObject.put("lat_15", String.valueOf((LAT * System.currentTimeMillis())));
//		jsonObject.put("lon_15", String.valueOf((LON * System.nanoTime())));
//		jsonObject.put("temp_15", String.valueOf((TEMP * System.nanoTime())));
//
//		jsonObject.put("lat_16", String.valueOf((LAT * System.currentTimeMillis())));
//		jsonObject.put("lon_16", String.valueOf((LON * System.nanoTime())));
//		jsonObject.put("temp_16", String.valueOf((TEMP * System.nanoTime())));
//
//		jsonObject.put("lat_17", String.valueOf((LAT * System.currentTimeMillis())));
//		jsonObject.put("lon_17", String.valueOf((LON * System.nanoTime())));
//		jsonObject.put("temp_17", String.valueOf((TEMP * System.nanoTime())));
//
//		jsonObject.put("lat_18", String.valueOf((LAT * System.currentTimeMillis())));
//		jsonObject.put("lon_18", String.valueOf((LON * System.nanoTime())));
//		jsonObject.put("temp_18", String.valueOf((TEMP * System.nanoTime())));
//
//		jsonObject.put("lat_19", String.valueOf((LAT * System.currentTimeMillis())));
//		jsonObject.put("lon_19", String.valueOf((LON * System.nanoTime())));
//		jsonObject.put("temp_19", String.valueOf((TEMP * System.nanoTime())));
//
//		jsonObject.put("lat_20", String.valueOf((LAT * System.currentTimeMillis())));
//		jsonObject.put("lon_20", String.valueOf((LON * System.nanoTime())));
//		jsonObject.put("temp_20", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("op_name", "bridgeNextClosure");
		
		
		this.message_id++;
		String message = jsonObject.toJSONString()+"-"+this.message_id;
		setMessageLength(message);
		return message;
	}
	
	public String getMessage5000bytes(){

		long LAT = Thread.currentThread().getId();
		long LON = Thread.currentThread().getId();
		long TEMP = Thread.currentThread().getId();

		JSONObject jsonObject = new JSONObject();
		
		jsonObject.put("lat", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_1", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_1", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_1", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_2", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_2", String.valueOf((LAT * System.nanoTime())));
		jsonObject.put("temp_2", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_3", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_3", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_3", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_4", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_4", String.valueOf((LAT * System.nanoTime())));
		jsonObject.put("temp_4", String.valueOf((TEMP * System.nanoTime())));
//////
		jsonObject.put("lat_5", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_5", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_5", String.valueOf((TEMP * System.nanoTime())));
//
		jsonObject.put("lat_6", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_6", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_6", String.valueOf((TEMP * System.nanoTime())));
////
		jsonObject.put("lat_7", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_7", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_7", String.valueOf((TEMP * System.nanoTime())));
//
		jsonObject.put("lat_8", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_8", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_8", String.valueOf((TEMP * System.nanoTime())));
//
		jsonObject.put("lat_9", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_9", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_9", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_10", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_10", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_10", String.valueOf((TEMP * System.nanoTime())));

	jsonObject.put("lat_11", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_11", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_11", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_12", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_12", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_12", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_13", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_13", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_13", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_14", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_14", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_14", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_15", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_15", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_15", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_16", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_16", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_16", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_17", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_17", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_17", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_18", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_18", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_18", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_19", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_19", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_19", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_20", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_20", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_20", String.valueOf((TEMP * System.nanoTime())));
		
		
		jsonObject.put("lat_21", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_21", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_21", String.valueOf((TEMP * System.nanoTime())));
		
		
		jsonObject.put("lat_22", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_22", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_22", String.valueOf((TEMP * System.nanoTime())));
		
		
		jsonObject.put("lat_23", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_23", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_23", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_24", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_24", String.valueOf((LAT * System.nanoTime())));
		jsonObject.put("temp_24", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_25", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_25", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_25", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_26", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_26", String.valueOf((LAT * System.nanoTime())));
		jsonObject.put("temp_26", String.valueOf((TEMP * System.nanoTime())));
//////
		jsonObject.put("lat_27", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_27", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_27", String.valueOf((TEMP * System.nanoTime())));
//
		jsonObject.put("lat_28", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_28", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_28", String.valueOf((TEMP * System.nanoTime())));
////
		jsonObject.put("lat_29", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_29", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_29", String.valueOf((TEMP * System.nanoTime())));
//
		jsonObject.put("lat_29", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_29", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_29", String.valueOf((TEMP * System.nanoTime())));
//
		jsonObject.put("lat_30", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_30", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_30", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_31", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_31", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_31", String.valueOf((TEMP * System.nanoTime())));

	jsonObject.put("lat_32", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_32", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_32", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_33", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_33", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_33", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_34", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_34", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_34", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_35", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_35", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_35", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_36", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_36", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_36", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_37", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_37", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_37", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_38", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_38", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_38", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_39", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_39", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_39", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_40", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_40", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_40", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_41", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_41", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_41", String.valueOf((TEMP * System.nanoTime())));
		
		
		jsonObject.put("lat_42", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_42", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_42", String.valueOf((TEMP * System.nanoTime())));
		
		
		jsonObject.put("lat_43", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_43", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_43", String.valueOf((TEMP * System.nanoTime())));
		
		jsonObject.put("lat_44", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_44", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_44", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_45", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_45", String.valueOf((LAT * System.nanoTime())));
		jsonObject.put("temp_45", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_46", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_46", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_46", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_47", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_47", String.valueOf((LAT * System.nanoTime())));
		jsonObject.put("temp_47", String.valueOf((TEMP * System.nanoTime())));
//////
		jsonObject.put("lat_48", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_48", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_48", String.valueOf((TEMP * System.nanoTime())));
//
		jsonObject.put("lat_49", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_49", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_49", String.valueOf((TEMP * System.nanoTime())));
////
		jsonObject.put("lat_50", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_50", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_50", String.valueOf((TEMP * System.nanoTime())));
//
		jsonObject.put("lat_51", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_51", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_51", String.valueOf((TEMP * System.nanoTime())));
//
		jsonObject.put("lat_52", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_52", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_52", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_53", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_53", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_53", String.valueOf((TEMP * System.nanoTime())));

	jsonObject.put("lat_54", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_54", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_54", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_55", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_55", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_55", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_56", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_56", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_56", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_57", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_57", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_57", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_58", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_58", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_58", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_59", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_59", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_59", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_60", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_60", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_60", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_61", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_61", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_61", String.valueOf((TEMP * System.nanoTime())));
		
		jsonObject.put("lat_62", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_62", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_62", String.valueOf((TEMP * System.nanoTime())));
		
		jsonObject.put("lat_63", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_63", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_63", String.valueOf((TEMP * System.nanoTime())));
		
		jsonObject.put("lat_64", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_64", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_64", String.valueOf((TEMP * System.nanoTime())));
		
		jsonObject.put("op_name", "bridgeNextClosure");
		
		this.message_id++;
		String message = jsonObject.toJSONString()+"-"+this.message_id;
		setMessageLength(message);
		return message;
	}
	
	
	public String getMessage1700Bytes(){

		long LAT = Thread.currentThread().getId();
		long LON = Thread.currentThread().getId();
		long TEMP = Thread.currentThread().getId();

		JSONObject jsonObject = new JSONObject();
		
		jsonObject.put("lat", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_1", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_1", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_1", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_2", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_2", String.valueOf((LAT * System.nanoTime())));
		jsonObject.put("temp_2", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_3", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_3", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_3", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_4", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_4", String.valueOf((LAT * System.nanoTime())));
		jsonObject.put("temp_4", String.valueOf((TEMP * System.nanoTime())));
//////
		jsonObject.put("lat_5", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_5", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_5", String.valueOf((TEMP * System.nanoTime())));
//
		jsonObject.put("lat_6", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_6", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_6", String.valueOf((TEMP * System.nanoTime())));
////
		jsonObject.put("lat_7", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_7", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_7", String.valueOf((TEMP * System.nanoTime())));
//
		jsonObject.put("lat_8", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_8", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_8", String.valueOf((TEMP * System.nanoTime())));
//
		jsonObject.put("lat_9", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_9", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_9", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_10", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_10", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_10", String.valueOf((TEMP * System.nanoTime())));

	jsonObject.put("lat_11", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_11", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_11", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_12", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_12", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_12", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_13", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_13", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_13", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_14", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_14", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_14", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_15", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_15", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_15", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_16", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_16", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_16", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_17", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_17", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_17", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_18", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_18", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_18", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_19", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_19", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_19", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_20", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_20", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_20", String.valueOf((TEMP * System.nanoTime())));
		
		jsonObject.put("op_name", "bridgeNextClosure");
		
		
		this.message_id++;
		String message = jsonObject.toJSONString()+"-"+this.message_id;
		setMessageLength(message);
		return message;
	}
	
	public String getBigMessage(){

		long LAT = Thread.currentThread().getId();
		long LON = Thread.currentThread().getId();
		long TEMP = Thread.currentThread().getId();

		JSONObject jsonObject = new JSONObject();

		jsonObject.put("lat", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_1", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_1", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_1", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_2", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_2", String.valueOf((LAT * System.nanoTime())));
		jsonObject.put("temp_2", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_3", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_3", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_3", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_4", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_4", String.valueOf((LAT * System.nanoTime())));
		jsonObject.put("temp_4", String.valueOf((TEMP * System.nanoTime())));
//////
		jsonObject.put("lat_5", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_5", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_5", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_6", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_6", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_6", String.valueOf((TEMP * System.nanoTime())));
////
		jsonObject.put("lat_7", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_7", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_7", String.valueOf((TEMP * System.nanoTime())));
//
		jsonObject.put("lat_8", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_8", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_8", String.valueOf((TEMP * System.nanoTime())));
//
		jsonObject.put("lat_9", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_9", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_9", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_10", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_10", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_10", String.valueOf((TEMP * System.nanoTime())));
//
		jsonObject.put("lat_11", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_11", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_11", String.valueOf((TEMP * System.nanoTime())));
//
		jsonObject.put("lat_12", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_12", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_12", String.valueOf((TEMP * System.nanoTime())));
//
		jsonObject.put("lat_13", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_13", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_13", String.valueOf((TEMP * System.nanoTime())));
//
		jsonObject.put("lat_14", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_14", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_14", String.valueOf((TEMP * System.nanoTime())));
//
		jsonObject.put("lat_15", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_15", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_15", String.valueOf((TEMP * System.nanoTime())));
//
		jsonObject.put("lat_16", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_16", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_16", String.valueOf((TEMP * System.nanoTime())));
//
		jsonObject.put("lat_17", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_17", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_17", String.valueOf((TEMP * System.nanoTime())));
//
		jsonObject.put("lat_18", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_18", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_18", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_19", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_19", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_19", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("lat_20", String.valueOf((LAT * System.currentTimeMillis())));
		jsonObject.put("lon_20", String.valueOf((LON * System.nanoTime())));
		jsonObject.put("temp_20", String.valueOf((TEMP * System.nanoTime())));

		jsonObject.put("op_name", "bridgeNextClosure");
		
		
		this.message_id++;
		String message = jsonObject.toJSONString()+"-"+this.message_id;
		setMessageLength(message);
		return message;
	}
	
	
	public int getMessageLength(){
		
		return this.length;
	}
	
	public long getMessageID(){
		
		return this.message_id;
	}
	
	private void setMessageLength(String message) {
		
		ByteArrayOutputStream ostream = new ByteArrayOutputStream();
		try {
			ObjectOutputStream obStream = new ObjectOutputStream(ostream);
			obStream.writeObject(message);
			byte[] rawObject = ostream.toByteArray();
			ostream.close();
			this.length = rawObject.length;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public static void main(String[] args) {
	
		MessageGenerator msg = new MessageGenerator();
		msg.getMessage();
		System.out.println(" Size  "+msg.length);
		
	}
}
