package eu.chorevolution.vsb.test.generated;

import javax.xml.ws.Endpoint;

import org.restlet.Component;
import org.restlet.Server;
import org.restlet.data.Protocol;

import eu.chorevolution.vsb.gm.protocols.primitives.BcGmSubcomponent;
import eu.chorevolution.vsb.gm.protocols.rest.BcRestSubcomponent;



public class BCSoapSubcomponentEndpointPublisher {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//System.setProperty("javax.xml.bind.JAXBContext", "com.sun.xml.internal.bind.v2.ContextFactory");
		
		BindingComponent bc = new BindingComponent();
		
	
		bc.run();
		
		final BcGmSubcomponent apiRef = new BcRestSubcomponent(bc.bcConfiguration1, bc.gmServiceRepresentation);
		
		/* Endpoint.publish("http://localhost:1898/vsb/BCSoapSubcomponent", new BCSoapSubcomponentEndpoint(apiRef));

		   System.out.println("Service is published!");*/
	}

}
