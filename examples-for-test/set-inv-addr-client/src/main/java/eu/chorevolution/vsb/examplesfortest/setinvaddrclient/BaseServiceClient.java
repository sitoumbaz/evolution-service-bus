package eu.chorevolution.vsb.examplesfortest.setinvaddrclient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import eu.chorevolution.chors.Base;
import eu.chorevolution.chors.BaseService;

public class BaseServiceClient {

	public static void main(String[] args) {

		String bd_wsdl = "http://localhost:8834/BaseService/setinvaddr?wsdl";
		String bc = "http://localhost:8834/BaseService/setinvaddr";
		List<String> urls = new ArrayList<String>();
		BaseService service;
		try{
			
			service = (BaseService) (Base.getPort("BaseService", bd_wsdl, bc, BaseService.class));
			urls.add("http://127.0.0.1:9082");
			service.setInvocationAddress("dest_role_test", "dest_name_test", urls);
			
		}catch (IOException e){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
