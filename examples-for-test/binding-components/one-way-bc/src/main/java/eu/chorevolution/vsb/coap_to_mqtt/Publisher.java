package eu.chorevolution.vsb.coap_to_mqtt;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class Publisher {
	
	public static void main(String[] args){
		MqttClient client = null;
		try{
			
			client = new MqttClient("tcp://localhost:"
					+ "", MqttClient.generateClientId());
			client.connect();
			MqttMessage message = new MqttMessage();
			message.setPayload("Simple publishing".getBytes());
			client.publish("hello", message);
			client.disconnect();
			
		}catch (MqttException e){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
