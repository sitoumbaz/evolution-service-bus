package eu.chorevolution.vsb.monitor_test;

import java.io.IOException;

import javax.management.MBeanNotificationInfo;

public interface PremierMBean {

	public String getNom() throws IOException; 
	
	public int getValeur() throws IOException; 
	
	public void setValeur(int valeur) throws IOException;
	
	public void rafraichir() throws IOException;
	
	public abstract MBeanNotificationInfo[] getNotificationInfo() ;
}
