package eu.chorevolution.vsb.monitor_test;

import java.io.IOException;

import javax.management.AttributeChangeNotification;
import javax.management.MBeanNotificationInfo;
import javax.management.Notification;
import javax.management.NotificationBroadcasterSupport;

public class Premier extends NotificationBroadcasterSupport implements PremierMBean {
	
	private static String nom = "PremierMBean";
	private int valeur = 100;
	private static long numeroSequence = 0l;
	
	public Premier(){}
	
	@Override
	public String getNom() {
		// TODO Auto-generated method stub
		return nom;
	}

	@Override
	public int getValeur() {
		// TODO Auto-generated method stub
		return valeur;
	}

	@Override
	public synchronized void setValeur(int valeur){
		// TODO Auto-generated method stub
		final Notification notification = new AttributeChangeNotification(this, numeroSequence,System.currentTimeMillis(), "Update value","Valeur", "int", this.valeur, valeur);
		this.valeur = valeur;
		sendNotification(notification);
	}

	@Override
	public void rafraichir() {
		// TODO Auto-generated method stub
		System.out.println("Refresh data");
	}

	@Override
	public MBeanNotificationInfo[] getNotificationInfo(){
		// TODO Auto-generated method stub
		final String[] types = new String[] { AttributeChangeNotification.ATTRIBUTE_CHANGE };
		final String name = AttributeChangeNotification.class.getName();
		final String description = "An attribute have been modified";
		final MBeanNotificationInfo info = new MBeanNotificationInfo(types, name, description);
		return new MBeanNotificationInfo[] { info };
	}

}
