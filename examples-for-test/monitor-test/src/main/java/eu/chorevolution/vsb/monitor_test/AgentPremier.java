package eu.chorevolution.vsb.monitor_test;

import java.io.IOException;
import java.lang.management.ManagementFactory;


import javax.management.InstanceAlreadyExistsException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import javax.management.remote.JMXConnectorServer;
import javax.management.remote.JMXConnectorServerFactory;
import javax.management.remote.JMXServiceURL;

public class AgentPremier{

		
	public static void main(String[] args) {
		
		MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();
		ObjectName name = null;
		Premier premier = null;
		
		try {
			
			name = new ObjectName("eu.chorevolution.vsb.test.monitoring:type=PremierMBean");
			premier = new Premier();
			mBeanServer.registerMBean(premier, name);
			
			
//			Creating and starting MRI connector
			JMXServiceURL url = new JMXServiceURL("service:jmx:rmi:///jndi/rmi://localhost:9000/server");
			JMXConnectorServer connector = JMXConnectorServerFactory.newJMXConnectorServer(url, null, mBeanServer);
			System.out.println("Starting RMI Connector "+url);
			connector.start();
			
			System.out.println("Start simulation...");
			while(true){
				
				Thread.sleep(1000);
				premier.setValeur(premier.getValeur()+1);
				System.out.println("new value"+premier.getValeur());
			}
			
		} catch (
				  
				 MalformedObjectNameException | 
				 InstanceAlreadyExistsException | 
				 MBeanRegistrationException | 
				 NotCompliantMBeanException | 
				 InterruptedException | 
				 IOException e) {
			
			e.printStackTrace();
			
		}
	}
}
