package eu.chorevolution.vsb.autopilote_ardrone.camera;

import java.awt.image.BufferedImage;

import com.dronecontrol.droneapi.listeners.VideoDataListener;

public class VideoDataListenerFromDrone implements VideoDataListener {
	
	private ImagesQueue imagesQueue;
	
	public VideoDataListenerFromDrone(ImagesQueue imagesQueue){
		
		this.imagesQueue = imagesQueue;
	}
	
	 
	
	@Override
	public void onVideoData(BufferedImage img){
		// TODO Auto-generated method stub
		
		imagesQueue.publish(img);
		//imagesQueue.push(img);
	}

}
