package eu.chorevolution.vsb.autopilote_ardrone.handler;

import com.dronecontrol.droneapi.data.enums.LedAnimation;

import eu.chorevolution.vsb.autopilote_ardrone.camera.CameraViewer;

/**
 * Hello world!
 *
 */
public class Handler 
{
    
 
    public static void performAutopilote(Brain br){

        //Attempt to connect to the drone
        if (br.connect()) {
            try {
            	
                //Execute drone commands
            	
            	new CameraViewer(br.getImagesQueue()).start(); 
            	
            	commands(br);
            	

            } catch (Exception e) {
                e.printStackTrace();
            } finally{
            	
                br.disconnect();
            }

        } else{
        	
            System.out.println("No Drone Connection.");
        }

    }

    private static void commands(Brain br) {
    	
    	
    	  br.playLedAnimation(LedAnimation.BLING_GREEN, 10, 120);
//        br.takeoff().hold(7000);
//        br.forward(10).doFor(1000);
//        br.stay().hold(15000);
//        br.land();
    	
//    	final int OPP = 6;
//    	final int HOVER_TIME = 800;
//        final int flightTime = 1000;  
//        
//    	br.takeoff().hold(HOVER_TIME);
//    	br.up(20).hold(HOVER_TIME);
//    	br.forward(20).doFor(flightTime);
//    	br.backward(20).doFor(flightTime/OPP);
//    	br.stay().hold(HOVER_TIME);
//    	br.land();
    	
    	
    	
        
//    	br.goRight(20).doFor(flightTime);
//    	br.goLeft(20).doFor(flightTime/OPP);
//    	br.stay().hold(HOVER_TIME);
//        
//    	br.forward(20).doFor(flightTime);
//    	br.backward(20).doFor(flightTime/OPP);
//    	br.stay().hold(HOVER_TIME);
//        
//    	br.goRight(20).doFor(flightTime);
//    	br.goLeft(20).doFor(flightTime/OPP);
//    	br.stay().hold(HOVER_TIME);
//        
//    	br.backward(20).doFor(flightTime);
//    	br.forward(20).doFor(flightTime/OPP);
//    	br.stay().hold(HOVER_TIME);
//
//    	br.goHome();
//    	br.stay().hold(HOVER_TIME);
//    	br.land();
        
    }
    
    
}
