package eu.chorevolution.vsb.autopilote_ardrone.camera;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.autonomous4j.constants.Constants;
import org.autonomous4j.listeners.xyz.A4jVideoDataListener;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;


public class ImagesQueue {

	 private Queue<BufferedImage> imagesQueue;
	 private MqttClient client;
	 private MqttMessage msg;
	
	 
	public ImagesQueue(){
		
		
	    imagesQueue = new LinkedList<BufferedImage>();
		msg = new MqttMessage();
        msg.setQos(0); // Deliver at most once (fire & forget) - crucial for video feed
        
        try{
        	
            client = new MqttClient("tcp://"+Constants.MQTT_SERVER+":1883", MqttClient.generateClientId());
            client.connect();
            
        } catch (MqttException ex){
        	
            Logger.getLogger(A4jVideoDataListener.class.getName()).log(Level.SEVERE, null, ex);
        }
	} 
	
	public void push(BufferedImage image) {
		
	
		if (image != null) {
			
			this.imagesQueue.add(image);
			
			
		}else{
			
			
			System.out.println("Can not add new image");
		}
		

	}

	public BufferedImage pull() {

		BufferedImage image = this.imagesQueue.poll();
		return image;

	}
	
	
	public void publish(BufferedImage image){
		
		
        try {
			
        	msg.setPayload(getImageByte(image));
            msg.setQos(0);
        	client.publish(Constants.TOP_LEVEL_TOPIC+"/image", msg);
			
		} catch (MqttPersistenceException e) {
			
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		} catch (MqttException e){
			
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
	}
	
	public byte[] getImageByte(BufferedImage bufferedImage){
		
		byte[] image = ((DataBufferByte) bufferedImage.getData().getDataBuffer()).getData();
		return image;
		
	}
	
	public int size(){
		
		return this.imagesQueue.size();
	}
	
	public void removeAll(){
		
		this.imagesQueue.clear();
	}
	
	
	
	

}
