package eu.chorevolution.vsb.sensor_devices.actuator;

import org.json.simple.JSONObject;
import org.restlet.Client;
import org.restlet.Context;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.MediaType;
import org.restlet.data.Method;
import org.restlet.data.Protocol;

import eu.chorevolution.vsb.control_panel.main.ControlPanel;
import eu.chorevolution.vsb.sensor_devices.general.Device;

public class ParrotDroneActuator extends  Device{

	
	private boolean parrotDroneActivate = false;
	public ParrotDroneActuator(String type_device, String protocol, String port, String address) {
		super(type_device, protocol, port, address);
		// TODO Auto-generated constructor stub
	}
	
	public void activate(String command){
		
		JSONObject obj = new JSONObject();
        obj.put("lat", "57.6198.7361989724.11.972487");
        obj.put("lon", "57.7361989724");
        obj.put("alt", "57.7361989724");
        obj.put("command", command);
        String content = obj.toJSONString();
		String url = "http://"+getAddress()+":"+getPort();
		Response resp = post(url, content);
		String rep = resp.getEntityAsText();
		System.out.println("Drone response : "+rep);
		if(rep.equals("LAND")) {
			
			
			parrotDroneActivate = false;
		}
	}
	
private static Response post(String url, String content){
		
		Request request = new Request();
		request.setResourceRef(url);
		request.setMethod(Method.POST);
		request.setEntity(content, MediaType.APPLICATION_JSON);
		Context ctx = new Context();
		Client client = new Client(ctx, Protocol.HTTP);
		System.out.println("request: " + request);
		System.out.println("request data: " + request.getEntityAsText());
		return client.handle(request);
	}

public boolean isParrotDroneActivate() {
	return parrotDroneActivate;
}

public void setParrotDroneActivate(boolean parrotDroneActivate) {
	this.parrotDroneActivate = parrotDroneActivate;
}

}
