package eu.chorevolution.vsb.autopilote.handler;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Command {

	private String latitude = null;
	private String longitude = null;
	private String altitude = null;
	private String commad = null;
	private boolean valid = true;

	public Command(final String data) {

		parse(data);
	}

	private void parse(String data) {

		JSONParser parser = new JSONParser();
		JSONObject jsonObject = null;
		try {
			jsonObject = (JSONObject) parser.parse(data);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		if (!jsonObject.containsKey("lat")) {
			
			valid = false;
		}

		if (!jsonObject.containsKey("lon")) {
			
			valid = false;
		}

		if (!jsonObject.containsKey("alt")) {
			
			valid = false;
		}

		if (!jsonObject.containsKey("command")){
			
			valid = false;
		}
		
		if(valid){
			
			setLatitude((String) jsonObject.get("lat"));
			setLongitude((String) jsonObject.get("lon"));
			setAltitude((String) jsonObject.get("alt"));
			setCommad((String) jsonObject.get("command"));
			
		}
		
	}
    
	public boolean isCommad(){
		
		return valid;
	}
	
	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getAltitude() {
		return altitude;
	}

	public void setAltitude(String altitude) {
		this.altitude = altitude;
	}

	public String getCommad() {
		return commad;
	}

	public void setCommad(String commad) {
		this.commad = commad;
	}
}
