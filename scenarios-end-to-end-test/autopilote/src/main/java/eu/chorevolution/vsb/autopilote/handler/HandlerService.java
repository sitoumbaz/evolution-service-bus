package eu.chorevolution.vsb.autopilote.handler;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.autonomous4j.constants.Constants;
import org.restlet.Component;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.Restlet;
import org.restlet.data.MediaType;
import org.restlet.data.Method;
import org.restlet.data.Protocol;
import org.restlet.data.Status;

import com.dronecontrol.droneapi.data.enums.LedAnimation;

import eu.chorevolution.vsb.autopilote.camera.CameraViewer;

/**
 * Hello world!
 *
 */
public class HandlerService extends Restlet {

	private ExecutorService executor = null;
	private String data = null;

	public HandlerService() throws Exception {
		
		
		executor = Executors.newFixedThreadPool(10);
		Component component = new Component();
		component.getServers().add(Protocol.HTTP, Constants.HANDLER_PORT);
		component.getDefaultHost().attach("/", this);
		component.getServers().get(0).getContext().getParameters().add("maxTotalConnections", "-1");
		component.getServers().get(0).getContext().getParameters().add("maxThreads", "100");
		component.start();

	}

	

	@Override
	public void handle(Request request, Response response) {

		if (request.getMethod().equals(Method.POST)) {

			try {

				data = request.getEntity().getText().toString();
				System.out.println(" Received command data " + data);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			response.setAutoCommitting(false);
			final Response resp = response;
			response.setAutoCommitting(false);
			Status status = null;
			Command command = new Command(data);
			if (!new Command(data).isCommad()) {

				status = new Status(Constants.BAD_COMMAND);
				System.out.println(" Received command data not valid");
				
			} else {
				
				Brain br = new Brain();
				if(br.connect()){
					
					new CameraViewer(br.getImagesQueue()).start();
					commands(command.getCommad(), br);
					status = new Status(Constants.GOOD_COMMAND);
					resp.setStatus(status);
					resp.setStatus(Status.SUCCESS_OK);
					resp.setEntity("LAND)",
					MediaType.TEXT_PLAIN);
					resp.commit();
					
				}else{
					
					status = new Status(Constants.GOOD_COMMAND);
					resp.setStatus(status);
					resp.setStatus(Status.SUCCESS_OK);
					resp.setEntity("NOLAND)",
					MediaType.TEXT_PLAIN);
					resp.commit();
					System.out.println(" No drone connection");
				}
				
				
			}
			
		}

	}

	private static void commands(String command, Brain br) {
		
		switch (command) {
				case Constants.TAKEOFF_LAND:
					
					br.takeoff().hold(7000);
					br.stay().hold(15000);
					br.land();
					
					break;

				case Constants.BLINK:
					
					br.playLedAnimation(LedAnimation.BLING_GREEN, 10, 120);
					br.playLedAnimation(LedAnimation.BLINK_GREEN_RED, 10, 120);
					break;

				case Constants.MOVETO:
					
					br.takeoff().hold(7000);
					br.forward(10).doFor(1000);
					br.stay().hold(15000);
					br.land();
					
					break;

				default:
					
					System.out.println("Bad command receive");
					break;
				}


	}

}
