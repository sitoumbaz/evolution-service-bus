package eu.chorevolution.vsb.autopilote.camera;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.util.LinkedList;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.autonomous4j.constants.Constants;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;

import com.dronecontrol.droneapi.listeners.ReadyStateChangeListener.ReadyState;
import eu.chorevolution.vsb.autopilote.listener.VideoDataFromDrone;

public class ImagesQueue {

	 private Queue<BufferedImage> imagesQueue;
	 private MqttClient client;
	 private MqttMessage msg;
	
	 
	public ImagesQueue(){
		
		
	    imagesQueue = new LinkedList<BufferedImage>();
		msg = new MqttMessage();
        
        try{
        	System.out.println("tcp://"+Constants.MQTT_SERVER+":"+Constants.MQTT_PORT);
            client = new MqttClient("tcp://"+Constants.MQTT_SERVER+":"+Constants.MQTT_PORT, MqttClient.generateClientId());
            client.connect();
            System.out.println("MqttClient connect");
            
        } catch (MqttException ex){
        	
            Logger.getLogger(VideoDataFromDrone.class.getName()).log(Level.SEVERE, null, ex);
        }
	} 
	
	public void push(BufferedImage image) {
		
	
		if (image != null) {
			
			this.imagesQueue.add(image);
			
			
		}else{
			
			
			System.out.println("Can not add new image");
		}
		

	}

	public BufferedImage pull() {

		BufferedImage image = this.imagesQueue.poll();
		return image;

	}
	
	
	public void publish(BufferedImage image){
		
		
        try {
			
        	msg.setPayload(getImageByte(image));
        	msg.setQos(0); // Deliver at most once (fire & forget) - crucial for video feed
        	client.publish(Constants.TOP_LEVEL_TOPIC+"/image", msg);
        	print(getImageByte(image));
			
		} catch (MqttPersistenceException e) {
			
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		} catch (MqttException e){
			
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
	}
	
	public byte[] getImageByte(BufferedImage bufferedImage){
		
		byte[] image = ((DataBufferByte) bufferedImage.getData().getDataBuffer()).getData();
		print(image);
		return image;
		
	}
	
	public int size(){
		
		return this.imagesQueue.size();
	}
	
	public void removeAll(){
		
		this.imagesQueue.clear();
	}
	
	
	public void print(final byte[] tabBytes){
		
		//
		System.out.println("Starting image bytes");
		StringBuffer sb = new StringBuffer();
		sb.append("START \n");

		for (int i = 0; i < 100; i++) {
			sb.append(tabBytes[i] + " | ");
		}

		System.out.println(sb.toString());
	}
	

}
