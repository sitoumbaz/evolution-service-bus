package eu.chorevolution.vsb.gm.protocols.websocket;

import java.net.InetSocketAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Base64.Decoder;
import java.util.List;
import java.util.concurrent.SynchronousQueue;
import org.json.simple.JSONObject;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

import eu.chorevolution.vsb.gm.protocols.primitives.BcGmSubcomponent;
import eu.chorevolution.vsb.gmdl.utils.BcConfiguration;
import eu.chorevolution.vsb.gmdl.utils.Data;
import eu.chorevolution.vsb.gmdl.utils.GmServiceRepresentation;
import eu.chorevolution.vsb.gmdl.utils.Scope;


public class BcWebsocketSubcomponent  extends BcGmSubcomponent{
	
	private WebSocketServer webSocketServer = null;
	private WebSocketObserver  webSocketObserver = null;
	SynchronousQueue<String> buffer = null;
//	private MeasureAgent agentPost = null;
//	private MeasureAgent agentMget = null;
	private GmServiceRepresentation serviceRepresentation = null;
	
	public BcWebsocketSubcomponent(BcConfiguration bcConfiguration,GmServiceRepresentation serviceRepresentation) {
		
		super(bcConfiguration);
		// TODO Auto-generated constructor stub
		
		this.serviceRepresentation = serviceRepresentation;
		buffer = new SynchronousQueue<String>();
		switch (this.bcConfiguration.getSubcomponentRole()){
		case SERVER:
			
			URI uri = null;
			try {
				
//				agentMget = new MeasureAgent("timestamp_2",System.currentTimeMillis(),MonitorConstant.M2,MonitorConstant.timestamp_2_port_listener);
				uri = new URI("http://"+bcConfiguration.getSubcomponentAddress()+":"+bcConfiguration.getSubcomponentPort());
				webSocketObserver = new WebSocketObserver(uri);
				
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			break;
			
		case CLIENT:   
			
//			agentPost = new MeasureAgent("timestamp_3",System.currentTimeMillis(),MonitorConstant.MDefault,MonitorConstant.timestamp_2_port_listener);
			webSocketServer = new WebSocketServer( new InetSocketAddress(bcConfiguration.getServicePort()));
			
			break;
		default:
			break;
		}
	}

	@Override
	public void start() {
		// TODO Auto-generated method stub
		switch (this.bcConfiguration.getSubcomponentRole()){
		case SERVER:
			
			WebSocketObserverThread Observerthread = new WebSocketObserverThread(webSocketObserver,this, serviceRepresentation);
//			WebSocketObserverThread Observerthread = new WebSocketObserverThread(webSocketObserver,this, serviceRepresentation, agentMget);
			Observerthread.start();
			break;
			
		case CLIENT:   
			webSocketServer.start();
			WebSocketPushNotification notifier = new WebSocketPushNotification(webSocketServer, buffer);
//			WebSocketPushNotification notifier = new WebSocketPushNotification(webSocketServer, buffer, agentPost);
			notifier.start();
			break;
		default:
			break;
		}
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postOneway(String destination, Scope scope, List<Data<?>> data, long lease) {
		// TODO Auto-generated method stub
		
			
			JSONObject  jsonObject = new JSONObject();
			for(Data<?> d : data) {
				
				// jsonObject.put(d.getName(), d.getObject().toString());
				
				try {
				 // byte[] datainbyte = Base64.decodeBase64(d.getObject().toString());
					
			
				//  byte[] messageInByte = Base64.decode(d.getObject().toString());
					String message = d.getObject().toString();
					buffer.put(message);
				} catch (InterruptedException e){
					
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
		
	}

	@Override
	public void mgetOneway(Scope scope, Object exchange) {
		// TODO Auto-generated method stub
		
		this.nextComponent.postOneway(this.bcConfiguration.getServiceAddress(), scope, (List<Data<?>>)exchange, 0);
		
	}

	@Override
	public void xmgetOneway(String source, Scope scope, Object exchange) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <T> T postTwowaySync(String destination, Scope scope, List<Data<?>> datas, long lease) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void xtgetTwowaySync(String destination, Scope scope, long timeout, Object response) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <T> T mgetTwowaySync(Scope scope, Object exchange) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void postTwowayAsync(String destination, Scope scope, List<Data<?>> data, long lease) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void xgetTwowayAsync(String destination, Scope scope, Object response) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mgetTwowayAsync(Scope scope, Object exchange) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postBackTwowayAsync(String source, Scope scope, Data<?> data, long lease, Object exchange) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setGmServiceRepresentation(GmServiceRepresentation serviceRepresentation) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public GmServiceRepresentation getGmServiceRepresentation() {
		// TODO Auto-generated method stub
		return null;
	}

}
