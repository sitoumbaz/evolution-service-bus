package eu.chorevolution.vsb.gmdl.utils.enums;

public enum OperationType {
  ONE_WAY,
  TWO_WAY_SYNC,
  TWO_WAY_ASYNC,
  STREAM;
}
