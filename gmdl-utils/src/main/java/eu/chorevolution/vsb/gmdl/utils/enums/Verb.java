package eu.chorevolution.vsb.gmdl.utils.enums;

public enum Verb {
  GET,
  POST,
  PUT,
  PATCH,
  DELETE;
}
